const jade = require('jade');
const less = require('less');
const fs = require('fs-extra');

const cwd = process.cwd();
const appDir = `${cwd}/app`;
const viewsDir = `${appDir}/views`;
const buildDir = `${cwd}/build`;
const staticBuildDir = `${buildDir}/static`;

const buildPage = (pageDirName) => {
  const pageDir = `${viewsDir}/${pageDirName}`;
  const data = require(`${pageDir}/data`);
  const template = jade.compileFile(`${pageDir}/index.jade`);
  const html = template(data);
  const pageDirPath = `${buildDir}/${pageDirName}`;

  fs.ensureDirSync(pageDirPath);
  return fs.writeFile(`${pageDirPath}/index.html`, html, 'utf8');
};

const buildCss = async () => {
  const style = fs.readFileSync(`${appDir}/style/app.less`, 'utf8');
  const output = await less.render(style);
  const cssDir = `${staticBuildDir}/css`;

  fs.ensureDirSync(cssDir);
  return fs.writeFile(`${cssDir}/app.css`, output.css, 'utf8');
};

const copyImages = () => {
  return fs.copy(`${appDir}/images`, `${staticBuildDir}/img`);
};

const copyVendor = () => {
  return fs.copy(`${appDir}/vendor`, `${buildDir}/vendor`);
};

const pagesSubDirs = fs.readdirSync(viewsDir, { withFileTypes: true })
                       .filter((dirent) => dirent.isDirectory() && dirent.name !== 'template')
                       .map((dirent) => dirent.name);
const pagesDirs = ['', ...pagesSubDirs];

Promise.all(pagesDirs.map((pageDir) => buildPage(pageDir)).concat(buildCss()).concat(copyImages()).concat(copyVendor()))
  .then(() => {
    console.log('Build Successful!');
  })
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
