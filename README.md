My Portfolio
============

The code behind my professional profile site.
---------------------------------------------

## Quickstart
```bash
docker-compose up
```
The website will be visible at http://localhost:8080/

If this fails while running a VPN, check the docker-compose.override.example.yml file for help.

## Install Dependencies
- `nvm install`
- `nvm use`
- `npm ci`

## Build It
- `npm run build`
- `npm start`

## Infrastructure
Terraform configuration and documentation located at [./infrastructure](/infrastructure/README.md)
