FROM node:14.15.4-alpine3.12

WORKDIR /app

COPY . .

#RUN npm ci --silent

ENTRYPOINT ["./entrypoint.sh"]
