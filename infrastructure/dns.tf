data "digitalocean_domain" "app_domain" {
  name = var.APP_DOMAIN
}

resource "digitalocean_record" "app_subdomains" {
  domain = data.digitalocean_domain.app_domain.name
  type   = "A"
  name   = var.APP_SUBDOMAIN
  value  = module.static_website.floating_ip
}

resource "digitalocean_record" "root_domain_asterisk" {
  domain = data.digitalocean_domain.app_domain.name
  type   = "A"
  name   = "*"
  value  = module.static_website.floating_ip
}

resource "digitalocean_record" "root_domain_at" {
  domain = data.digitalocean_domain.app_domain.name
  type   = "A"
  name   = "@"
  value  = module.static_website.floating_ip
}
