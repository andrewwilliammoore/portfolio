variable "GITLAB_TOKEN" {}
variable "DIGITAL_OCEAN_PERSONAL_ACCESS_TOKEN" {}
variable "APP_DOMAIN" {}
variable "APP_SUBDOMAIN" {}
variable "ACME_REGISTRATION_EMAIL" {}
variable "INSTANCE_PUBLIC_SSH_KEY" {}
