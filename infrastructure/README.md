Infrastructure Configuration
============

Uses Terraform
---------------------------------------------

## Getting Started
CD into this directory
```bash
cd infrastructure
```

Create a gitignored file `.envrc` with the following:
```bash
#!/usr/bin/env bash

# Gitlab for CI
# Personal access token with permissions read_user api read_api read_repository, write_repository
export TF_VAR_GITLAB_TOKEN=TOKEN_FROM_GITLAB
# Digital Ocean for DNS
export TF_VAR_DIGITAL_OCEAN_PERSONAL_ACCESS_TOKEN=...;
# DNS settings
export TF_VAR_APP_DOMAIN=...; # Root domain for the app. This is assumed to already exist
export TF_VAR_APP_SUBDOMAIN=...; # subdomain for accessing the website
# For letsencrypt
export TF_VAR_ACME_REGISTRATION_EMAIL=...;
# For HTTP Proxy
export TF_VAR_INSTANCE_PUBLIC_SSH_KEY=...;

# Openstack
# These values can be found from your openstack provider, such as from a downloadable openrc.sh file.
# Note that these specific values can vary based on cloud provider and their own authentication, etc.

# To use an OpenStack cloud you need to authenticate against the Identity
# service named keystone, which returns a **Token** and **Service Catalog**.
# The catalog contains the endpoints for all services the user/tenant has
# access to - such as Compute, Image Service, Identity, Object Storage, Block
# Storage, and Networking (code-named nova, glance, keystone, swift,
# cinder, and neutron).
export OS_AUTH_TYPE="v3applicationcredential"
export OS_AUTH_URL=OS_AUTH_URL
export OS_IDENTITY_API_VERSION=3

# Note that these are provided by OVH Cloud but not necessarily other providers
export OS_USER_DOMAIN_NAME=${OS_USER_DOMAIN_NAME:-"Default"}
export OS_PROJECT_DOMAIN_NAME=${OS_PROJECT_DOMAIN_NAME:-"Default"}

# Note that these are provided by OVH Cloud but not necessarily other providers
# With the addition of Keystone we have standardized on the term **tenant**
# as the entity that owns the resources.
export OS_TENANT_ID=...;
export OS_TENANT_NAME=...;
export OS_USERNAME=...;

# With Keystone authentication you pass the keystone password.
export OS_PASSWORD=...;

# If your configuration has multiple regions, we set that information here.
# OS_REGION_NAME is optional and only valid in certain environments.
export OS_REGION_NAME=OS_REGION_NAME
# Don't leave a blank variable, unset it if it was empty
if [ -z "$OS_REGION_NAME" ]; then unset OS_REGION_NAME; fi

# Used by OVH terraform provider
export OVH_ENDPOINT=ovh-ca # This might vary
# NOTE: needed to create api keys on https://ca.ovh.com/auth/api/createToken
export OVH_APPLICATION_KEY=...;
export OVH_APPLICATION_SECRET=...;
export OVH_CONSUMER_KEY=...;
```

Load environment variables
```bash
. ./.envrc
```

Create an object storage with your provider that will hold the terraform state. Name it `portfolio-backend`.

Initialize Terraform
```bash
tfenv install
tfenv use
terraform init
```

Apply Infrastructure
```bash
terraform apply
```

## Troubleshooting
For object storage containers, use the `swift` client. A team credential needs to be created with the cloud provider, and seems to need click-ops. Use that credential's `openrc.sh` and `swift list`, for example.
