terraform {
  required_providers {
    ovh = {
      source  = "ovh/ovh"
      version = "0.34.0"
    }

    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.49.0"
    }

    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.4.0"
    }

    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.8.0"
    }

    acme = {
      source  = "vancluever/acme"
      version = "~> 2.8.0"
    }

    local = {
      source  = "hashicorp/local"
      version = "~> 2.2.2"
    }
  }
}

provider "ovh" {
  # Configuration options
}

provider "openstack" {
  region = "GRA11"
}

provider "gitlab" {
  token = var.GITLAB_TOKEN
}

provider "digitalocean" {
  token = var.DIGITAL_OCEAN_PERSONAL_ACCESS_TOKEN
}

provider "acme" {
  # server_url = "https://acme-staging-v02.api.letsencrypt.org/directory"
  server_url = "https://acme-v02.api.letsencrypt.org/directory"
}
