data "openstack_networking_network_v2" "public" {
  name = "Ext-Net"
}

module "static_website" {
  source = "https://andrewwilliammoore.gitlab.io/terraform-modules/openstack/static-website-with-gitlab-ci/1.0.1.zip"

  providers = {
    openstack = openstack
    gitlab    = gitlab
    acme      = acme
    local     = local
  }

  gitlab_project_id       = data.gitlab_project.this.id
  fqdn                    = "${var.APP_SUBDOMAIN}.${var.APP_DOMAIN}"
  app_name                = "portfolio"
  proxy_public_ssh_key    = var.INSTANCE_PUBLIC_SSH_KEY
  provider_storage_domain = "storage.de.cloud.ovh.net"
  acme_registration_email = var.ACME_REGISTRATION_EMAIL
  dns_challenge_provider  = "digitalocean"
  public_network_name     = data.openstack_networking_network_v2.public.name
  object_storage_region   = "DE"

  dns_challenge_config = {
    DO_AUTH_TOKEN = var.DIGITAL_OCEAN_PERSONAL_ACCESS_TOKEN
  }
}
