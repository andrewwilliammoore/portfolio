const title = "Andrew Moore's Projects";
const projects = [
  {
    image: '/static/img/projects/babbel-b2b.jpg',
    url: 'https://www.babbelforbusiness.com',
    title: 'Babbel GmbH, B2B',
    description: "Developing applications for account management for the business-to-business department " +
                 "of the world's leading language-learning service. Working with event-driven data " +
                 "for generating usage reports for business customers.",
    tags: ['Terraform', 'AWS', 'Ruby', 'Rails', 'React', 'Python'],
    git: 'https://github.com/babbel'
  },
  {
    image: '/static/img/projects/babbel.jpg',
    url: 'https://www.babbel.com',
    title: 'Babbel GmbH, Marketing Tech',
    description: "Working with all areas of the stack to enable content management via CMS for " +
                 "Babbel's marketing pages. Enabled A/B testing for user conversion data.",
    tags: ['Terraform', 'Node', 'AWS', 'Ruby', 'React', 'Contentful'],
    git: 'https://github.com/babbel'
  },
  {
    image: '/static/img/projects/spoonflower.jpg',
    url: 'https://www.spoonflower.com',
    title: 'Spoonflower GmbH',
    description: "Internationalizing the online retail shop of the number one design market place " +
                 "and fabric printing business. Helped to add support for multiple languages, currencies, and " +
                 "measuring systems.",
    tags: ['Ruby', 'Rails', 'React', 'Redux', 'Python', 'AWS'],
    git: 'https://github.com/spoonflower'
  },
  {
    image: '/static/img/projects/milligram.jpg',
    url: 'https://www.milligramrx.com',
    title: 'Milligram',
    description: "Building a service for aggregating search results for prescription medication pricing.",
    tags: ['Ruby', 'Rails', 'Heroku', 'jQuery']
  },
];

module.exports = { title, projects };
